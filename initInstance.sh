#!/bin/bash
sudo apt-get update
sudo apt-get install -y apache2 python

# download & install code
REPO_DIR="/home/ubuntu/462_web_server"
git clone https://bitbucket.org/bergbria/462_web_server.git $REPO_DIR
cd $REPO_DIR
chmod +x linux_greedy_install.sh
./linux_greedy_install.sh